﻿ using UnityEngine;
 using UnityEditor;
  
 public class TexturePreProcessor : AssetPostprocessor
 {
     void OnPreprocessTexture()
     {
         TextureImporter importer = assetImporter as TextureImporter;
         Object asset = AssetDatabase.LoadAssetAtPath(importer.assetPath, typeof(Texture2D));
         if (!asset)
         {
             importer.textureFormat = TextureImporterFormat.AutomaticTruecolor;
         }         
     }
 }