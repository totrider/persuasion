﻿using UnityEngine;
using System.Collections;

public class HideBigFeedback : MonoBehaviour {

    public GameObject bigHelperOBJ, minimizedHelpherOBJ;
    public CanvasGroup bigHelperCanvasGroup, minimizedHelperCanvasGroup;
    public Interaction interactionScript;


    public void HideBigHelper()
    {
        interactionScript.regionIsOccluded = false;
        bigHelperOBJ = GameObject.Find("BigHelper");
        bigHelperCanvasGroup = bigHelperOBJ.GetComponent<CanvasGroup>();
        bigHelperCanvasGroup.alpha = 0;
        bigHelperCanvasGroup.interactable = false;
        bigHelperCanvasGroup.blocksRaycasts = false;
    }

    public void ShowMinimizedHelper()
    {
        minimizedHelpherOBJ = GameObject.Find("MinimizedHelper");
        minimizedHelperCanvasGroup = minimizedHelpherOBJ.GetComponent<CanvasGroup>();
        minimizedHelperCanvasGroup.alpha = 1;
        minimizedHelperCanvasGroup.interactable = true;
        minimizedHelperCanvasGroup.blocksRaycasts = true;
    }

}
