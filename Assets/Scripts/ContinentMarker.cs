﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContinentMarker : MonoBehaviour
{
    private Text percentageText, timeText;
    public float percentage, countdown, countdownMax;
    public bool isBuilding;
    public string continentName = "";
    public GameObject regionMarkerObjectA, regionMarkerObjectB, regionMarkerObjectC, percentageTextOBJ, timeTextOBJ;
    public RegionMarker regionMarkerScriptA, regionMarkerScriptB, regionMarkerScriptC;


    // Use this for initialization
    void Start()
    {
        percentageText = percentageTextOBJ.GetComponent<Text>();

        if (timeTextOBJ != null)
        {
            timeText = timeTextOBJ.GetComponent<Text>();
        }


        regionMarkerScriptA = regionMarkerObjectA.GetComponent<RegionMarker>();
        regionMarkerScriptB = regionMarkerObjectB.GetComponent<RegionMarker>();
        regionMarkerScriptC = regionMarkerObjectC.GetComponent<RegionMarker>();
    }

    // Update is called once per frame
    void Update()
    {
        percentage = (regionMarkerScriptA.percentage + regionMarkerScriptB.percentage + regionMarkerScriptC.percentage) / 3.0f;

        if (countdown <= 0)
        {
            countdownMax = (regionMarkerScriptA.countdown + regionMarkerScriptB.countdown + regionMarkerScriptC.countdown);
        }

        countdown = (regionMarkerScriptA.countdown + regionMarkerScriptB.countdown + regionMarkerScriptC.countdown);

        if (countdown > 0)
        {
            countdown -= Time.deltaTime;
            if (timeText != null)
            {
                timeText.text = countdown.ToString("F2");
                percentageText.text = Mathf.Ceil(percentage) + "%";
            }
        }
        else
        {
            countdown = 0;
            countdownMax = 0;
            if (timeText != null)
            {
                timeText.text = "";
            }
            percentageText.text = Mathf.Ceil(percentage) + "%";
        }

    }
}