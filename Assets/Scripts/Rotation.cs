﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {

    public float rotationSpeed;
    Vector3 currentRotation;

	void Update () {

        currentRotation = transform.eulerAngles;
        transform.eulerAngles = currentRotation + new Vector3(0, rotationSpeed * Time.deltaTime, 0);
    }
}
