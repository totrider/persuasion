﻿using UnityEngine;
using System.Collections;

public class Dropdown : MonoBehaviour {

    public bool isToggled;
    public CanvasGroup dropdownCanvasGroup;

    public void DropdownToggle()
    {
        if (isToggled == false)
        {
            dropdownCanvasGroup.alpha = 1f;
            dropdownCanvasGroup.blocksRaycasts = true;
            dropdownCanvasGroup.interactable = true;
            isToggled = true;
        }
        else
        {
            isToggled = false;
            dropdownCanvasGroup.alpha = 0f;
            dropdownCanvasGroup.blocksRaycasts = false;
            dropdownCanvasGroup.interactable = false;
        }
    }
}
