﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VersionManager : MonoBehaviour {

    public bool isConstructive = true;
    public Toggle versionToggle;
    public GameObject versionToggleOBJ;

    public void Update()
    {

        if (versionToggle == null && GameObject.Find("VersionToggle") == true)
        {
            versionToggleOBJ = GameObject.Find("VersionToggle");
            versionToggle = versionToggleOBJ.GetComponent<Toggle>();
        }
        else if (versionToggle != null)
        {
            if (versionToggle.isOn == true)
            {
                isConstructive = true;
            }
            else
            {
                isConstructive = false;
            }
        }
    }
}
