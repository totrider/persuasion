﻿using UnityEngine;
using System.Collections;

public class GameObjectInitialiser : MonoBehaviour {

    public GameObject gameManager;


	void Start () {

        if (GameObject.Find("GameManager(Clone)") == false)
        {
            Vector3 pos = new Vector3(0, 0, 0);
            Instantiate(gameManager, pos, Quaternion.identity);
        }
    }
}
