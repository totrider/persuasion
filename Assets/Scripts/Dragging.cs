﻿using UnityEngine;
using System.Collections;

public class Dragging : MonoBehaviour {
    private float yRotation = 0f, getInitialCoord, getLastCoord;
    public GameObject mainCamera;
    public Interaction interactionScript;
   
    public float sensitivity = 5.0f; // controls rotation speed

    // Update is called once per frame
    void Update () {

        interactionScript = mainCamera.GetComponent<Interaction>();

        if (Interaction.isZoomedIn == false)
        {
        // On mouse click down
        if (Input.GetMouseButtonDown(0))
        {
                // get mouse coordinates of the X axis           
                getInitialCoord = Input.mousePosition.x;
             
        }
        // On mouse hold down 
        if (Input.GetMouseButton(0))
        {
            // get newest mouse coordinate
            getLastCoord = Input.mousePosition.x;

            //calculate difference between Initial mouse coordinate and new one

            yRotation = getLastCoord - getInitialCoord;
            //yOffset = transform.rotation.y;


            getInitialCoord = getLastCoord;

            // Rotate 
            transform.Rotate(new Vector3(0, -(yRotation*sensitivity * Time.deltaTime), 0));                               
        }
        }
    }
}

