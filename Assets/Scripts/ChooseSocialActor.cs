﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChooseSocialActor : MonoBehaviour {



	public void SetFemale()
	{
		PlayerPrefs.SetString ("GenderChosen", "female");
	}

	public void SetMale()
	{
		PlayerPrefs.SetString ("GenderChosen", "male");
	}

	public void GetSocialActor()
	{
		if (PlayerPrefs.GetString ("GenderChosen").Equals( "female")) 
		{
			GameObject.Find ("Male").SetActive (false);
			GameObject.Find ("Female").SetActive (true);
            GameObject.Find("MaleBig").SetActive(false);
            GameObject.Find("FemaleBig").SetActive(true);

        } 
		else 
		{
			GameObject.Find ("Male").SetActive(true); // default
			GameObject.Find ("Female").SetActive(false);
            GameObject.Find("MaleBig").SetActive(true); // default
            GameObject.Find("FemaleBig").SetActive(false);
        }
	}

	void Awake()
	{
		if (Application.loadedLevelName.Equals ("Game"))
		{
			GetSocialActor ();
		}
	}
}
