﻿using UnityEngine;
using System.Collections;

public class Region : MonoBehaviour {

    public float percentage = 0, percentageIncrement = -5, countdown = 0, incrementerModifier = 1, incrementCheckTemp;
    public int bonusScore = 0, incrementCheck = 0, penaltyScore = 0;
    public GameObject continentMarkerObject, timeSimulatorOBJ, scoreOBJ;
    public ContinentMarker associatedContinent;
    public TimeSimulator timeSimulatorScript;
    public Score scoreScript;
    public bool bonusOptained, penaltyOptained;
    public ConstructionOptions constructionOptionsScript;


    [Range(1, 9)]
    public int choiceA;

    [Range(0, 120)]
    public float countdownA = 1;

    [Range(1, 9)]
    public int choiceB;

    [Range(0, 120)]
    public float countdownB = 1;

    [Range(1, 9)]
    public int choiceC;

    [Range(0, 120)]
    public float countdownC = 1;

    void Start ()
    {
        associatedContinent = continentMarkerObject.GetComponent<ContinentMarker>();

        timeSimulatorScript = timeSimulatorOBJ.GetComponent<TimeSimulator>();

        scoreScript = scoreOBJ.GetComponent<Score>();
    }

    void Update()
    {

        // do a countdown
        if (countdown > 0)
        {
            countdown -= Time.deltaTime;
        }
        else
        {
            countdown = 0;
        }
        //time += Time.deltaTime * simulationSpeed;

        incrementCheck = Mathf.FloorToInt(timeSimulatorScript.time / timeSimulatorScript.durationOfAYear);

        if (incrementCheck >= incrementCheckTemp + 1f && bonusOptained == false)
        {
            incrementCheckTemp = incrementCheck;
            percentage += percentageIncrement;

            if (percentage >= 100f)
            {
                percentage = 100f;

                if (bonusOptained == false)
                {
                    scoreScript.score += bonusScore;
                    bonusOptained = true;
                }
            }
            else if (percentage <= 0f)
            {
                percentage = 0f;
                if (penaltyOptained == false)
                {
                    scoreScript.score += penaltyScore;
                    penaltyOptained = true;
                }
            }
            else if (percentage > 0f)
            {
                penaltyOptained = false;
            }
        }
    }
}


