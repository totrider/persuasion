﻿using UnityEngine;
using System.Collections;


public class DraggingAlternative : MonoBehaviour {
    private float yRotation; 
    private float getInitialCoord;
    private float getLastCoord;
    private float getEuler;
        //public float sensitivity = 0.005f; // controls how sensitive the rotation. If transform.rotate use this
    public float sensitivity = 0.5f; // controls how sensitive the rotation. If transform.eurelAngles use this
    // Update is called once per frame
    void Update () {
        // On mouse click down
        if (Input.GetMouseButtonDown(0))
        {
            // get current euler angle and mouse coordinates of the X axis  
            getEuler = transform.eulerAngles.y;
                getInitialCoord = Input.mousePosition.x;
             
        }
        // On mouse hold down 
        if (Input.GetMouseButton(0))
        {
            // get newest mouse coordinate
            getLastCoord = Input.mousePosition.x;

            //calculate difference between Initial mouse coordinate and new one
             yRotation = getLastCoord - getInitialCoord;
            

           

           // transform.Rotate(new Vector3(0, -(yRotation*sensitivity), 0));

            transform.eulerAngles = new Vector3(0, getEuler + -(yRotation * sensitivity), 0);


        


}

        

    }
}

