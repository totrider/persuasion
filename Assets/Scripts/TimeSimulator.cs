﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimeSimulator : MonoBehaviour {

    private Text text;
    public float time = 0.0f;
    private float minutes, endTime, monthsInASeason, monthsInAYear, seasonCheck, daysInAYear, durationOfAMonth, durationOfADay;
    private int month = 1, season = 1, day = 1, year;
    private string monthName, seasonName;

    [Tooltip("The year at which the player will start")]
    public float startYear = 0;

    [Tooltip("The amount of years it takes from start to when the game ends")]
    public float gameDuration = 20f;

    [Tooltip("The duration of a year in seconds")]
    public float durationOfAYear = 60f;

    [Tooltip("The amount of seasons in a year")]
    public float seasonsInAYear = 4f;

    [Tooltip("The amount of days in a month")]
    public int daysInAMonth = 30;

    [Tooltip("The amount of days in a month")]
    public float simulationSpeed = 1f;

    [Tooltip("Display all months or only seasons")]
    public bool simplifiedCalender = true;

    void Start () {

        if (simplifiedCalender == true)
        {
            monthsInASeason = 1f;
        }
        else
        {
            monthsInASeason = 3f;
        }

        monthsInAYear = monthsInASeason * seasonsInAYear;
        text = GetComponent<Text>();
    }

    void Update () {

        time += Time.deltaTime * simulationSpeed;

        year = Mathf.FloorToInt (time / durationOfAYear);

        season = Mathf.FloorToInt (time / (durationOfAYear / seasonsInAYear)) % (int)seasonsInAYear + 1;

        month = Mathf.FloorToInt(time / (durationOfAYear / seasonsInAYear / monthsInASeason)) % (int)monthsInAYear + 1;

        day = Mathf.FloorToInt(time / (durationOfAYear / seasonsInAYear / monthsInASeason / daysInAMonth)) % (int)daysInAMonth + 1;

        if (monthsInASeason > 0 && monthsInASeason < 4)
        {
            switch (month)
            {
                case 1:
                    monthName = "March";
                    break;
                case 2:
                    monthName = "April";
                    break;
                case 3:
                    monthName = "May";
                    break;
                case 4:
                    monthName = "June";
                    break;
                case 5:
                    monthName = "Juli";
                    break;
                case 6:
                    monthName = "August";
                    break;
                case 7:
                    monthName = "September";
                    break;
                case 8:
                    monthName = "October";
                    break;
                case 9:
                    monthName = "November";
                    break;
                case 10:
                    monthName = "December";
                    break;
                case 11:
                    monthName = "January"; 
                    break;
                case 12:
                    monthName = "February"; 
                    break;
                default:
                    Debug.Log("month not found");
                    break;
            }
        }

        switch (season)
        {
            case 1:
                seasonName = "Spring";
                break;
            case 2:
                seasonName = "Summer";
                break;
            case 3:
                seasonName = "Autumn";
                break;
            case 4:
                seasonName = "Winter";
                break;
            default:
                Debug.Log("season not found");
                break;
        }

        text.text = "Year: " + (year + startYear) + "\n" + "Season: " + seasonName + "\n";
        if (monthsInASeason > 1)
        {
            text.text += "Month: " + monthName + "\n";
        }

        text.text += "Day: " + day;

        //load game over scene
        if (year >= gameDuration)
        {         
            SceneManager.LoadScene("Congratulations");
        }
    }
}

