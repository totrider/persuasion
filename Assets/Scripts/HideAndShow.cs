﻿using UnityEngine;
using System.Collections;

public class HideAndShow : MonoBehaviour
{
    public CanvasGroup CanvasGroup;

    public void Hide()
    {
        CanvasGroup.alpha = 0;
        //CanvasGroup.interactable = false;
        //CanvasGroup.blocksRaycasts = false;
    }

    public void Show()
    {
        CanvasGroup.alpha = 1;
        //CanvasGroup.interactable = true;
        //CanvasGroup.blocksRaycasts = true;
    }
}