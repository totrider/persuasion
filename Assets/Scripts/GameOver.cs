﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {


	private Text text;

    public GameObject gameManager;
    public VersionManager versionManagerScript;


	// Use this for initialization
	void Start () 
	{
        gameManager = GameObject.Find("GameManager(Clone)");
        versionManagerScript = gameManager.GetComponent<VersionManager>();

        text = GetComponent<Text>();
        if (versionManagerScript.isConstructive)
        {
            text.text = "Congratulations! \n You have preserved the world by: " + "<color=green><b>" + PlayerPrefs.GetInt("PercentageTotal") + "%</b></color>" + "\n Your score is: \n" + "<color=green><b>" + PlayerPrefs.GetInt("Score") + "</b></color>";
        }
        else
        {
            text.text = "Congratulations! \n You have destroyed the world by: " + "<color=green><b>" + PlayerPrefs.GetInt("PercentageTotal") + "%</b></color>" + "\n Your score is: \n" + "<color=green><b>" + PlayerPrefs.GetInt("Score") + "</b></color>";
        }

        PlayerPrefs.SetInt("Score", 0);
        PlayerPrefs.SetInt("PercentageTotal", 0);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
