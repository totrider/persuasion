﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{

    private Text text;
    public int score;

    public int GetScore()
    {
        return score;
    }

    void Start()
    {

        text = GetComponent<Text>();
    }

    void Update()
    {
        if (score <= 0)
        {
            score = 0;
        }

        text.text = "Score: " + "\n" + score;
        PlayerPrefs.SetInt("Score", score);
    }
}
