﻿using UnityEngine;
using System.Collections;

public class ProgressBar : MonoBehaviour {

    public float currentLength, currentHeight, maskWidth, maskHeight, maxHeight;
    public RectTransform Mask;
    public GameObject associatedValueObject;
    public RegionMarker regionMarkerScript;
    public ContinentMarker continentMarkerScript;
    public WorldProgress worldProgressScript;
    public Region regionScript;
    public bool isVertical;

	void Start()
    {
        maskWidth = Mask.sizeDelta.x;
        maskHeight = Mask.sizeDelta.y;

        if (associatedValueObject.GetComponent<RegionMarker>())
        {
            regionMarkerScript = associatedValueObject.GetComponent<RegionMarker>();
        }
        else if (associatedValueObject.GetComponent<ContinentMarker>())
        {
            continentMarkerScript = associatedValueObject.GetComponent<ContinentMarker>();
        }
        else if (associatedValueObject.GetComponent<WorldProgress>())
        {
            worldProgressScript = associatedValueObject.GetComponent<WorldProgress>();
        }
    }

	void Update ()
    {
        if (regionMarkerScript != null)
        {
            currentLength = regionMarkerScript.percentage;
        }
        else if (continentMarkerScript != null)
        {
            currentLength = continentMarkerScript.percentage;
            currentHeight = continentMarkerScript.countdown;
            maxHeight = continentMarkerScript.countdownMax;

        }
        else if (worldProgressScript != null)
        {
            currentLength = worldProgressScript.percentage;
        }
        else
        {
            currentLength = 0f;
        }

        if (isVertical == false)
        {
            Mask.sizeDelta = new Vector2(maskWidth * (currentLength / 100f), Mask.sizeDelta.y);
        }

        if (isVertical == true && currentHeight > 0)
        {
            Mask.sizeDelta = new Vector2(Mask.sizeDelta.x, -currentHeight * (maskHeight / maxHeight) + maskHeight);
        }
        else if (isVertical == true)
        {
            Mask.sizeDelta = new Vector2(Mask.sizeDelta.x, 0);
        }
    }
}
