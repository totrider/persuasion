﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ConstructionOptions : MonoBehaviour
{
    public int constructionChosen = -1;
    public Region selectedRegion;
    public ContinentMarker selectedContinent;
    public Score score;
    public Button choiceA, choiceB, choiceC;
    public Text textA, textB, textC, socialActorTextBig, socialActorTextSmall, HoverAPercentage, HoverAPerYear, HoverATime, HoverBPercentage, HoverBPerYear, HoverBTime, HoverCPercentage, HoverCPerYear, HoverCTime;
    public float tempCountdown;
    public int constructionChosenTemp;
    public GameObject scoreGUI, mainCamera, gameManager, bigHelperOBJ, minimizedHelpherOBJ;
    public Interaction interactionScript;
    public VersionManager versionManagerScript;
    public CanvasGroup bigHelperCanvasGroup, minimizedHelperCanvasGroup;
    public bool b1, b2, b3, b4, b5, b6, b7, b8, b9;


    void Start ()
    {
        score = scoreGUI.GetComponent<Score>();
        interactionScript = mainCamera.GetComponent<Interaction>();

        gameManager = GameObject.Find("GameManager(Clone)");
        versionManagerScript = gameManager.GetComponent<VersionManager>();

        //use the 3 gui buttons and pass on the variables from the selected region which is carried over through the interaction script
        choiceA.onClick.AddListener(() => {StartConstruction(selectedRegion.countdownA, selectedRegion.choiceA); });
        choiceB.onClick.AddListener(() => {StartConstruction(selectedRegion.countdownB, selectedRegion.choiceB); });
        choiceC.onClick.AddListener(() => {StartConstruction(selectedRegion.countdownC, selectedRegion.choiceC); });
    }

    public void Update ()
    {
        if (selectedRegion != null && versionManagerScript.isConstructive == true)
        {
            //environ topics order:
            //1,2,3 - shale gas
            //4,5,6 - forest
            //7,8,9 - water
            //text for choiceA
            if (selectedRegion.choiceA == 1)
            {
                textA.text = "This area is rich with <color=blue><b>shales</b></color>. <color=#004BFFFF><b>Shales</b></color> are <color=#FF5900FF><b>sedimentary rock</b></color> formations rich in organic matter and are often used for <color=#F76719FF><b>extracting</b></color> a dry <b>natural gas</b> composed largely of <color=black><b>methane</b></color>, also known as <b>shale gas</b>. Issue a protective law limiting the exploitation of shale gas.";

            }
            else if (selectedRegion.choiceA == 4)
            {
                textA.text = "<b>Plant new trees</b> on the open lands, which is also know as <b>afforestation</b>.";
            }
            else if (selectedRegion.choiceA == 7)
            {
                textA.text = "The local factory is dumping its waste directly into the nearby lake, meaning the lake is being polluted by a <b>point source</b>, since the pollution stems from <b>one point only</b>. Fine the factory to stop their <b>point source pollution</b>.";
            }

            //text for choiceB
            if (selectedRegion.choiceB == 2)
            {
                textB.text = "<b>Fracking</b> is a process in which large volumes of <b>freshwater</b>, <b>sand</b>, and <b>chemicals</b> are injected into the ground, which in turn <b>creates fractures</b> in shale formations, allowing the <b>shale gas</b> to escape and be recovered. Limit the use of fracking platforms across the area.";
            }
            else if (selectedRegion.choiceB == 5)
            {
                textB.text = "Slow the rate of <b>greenhouse gas</b> emissions by creating more forests, since they normally <b>absorb CO2</b> through <b>photosynthesis</b>.";
            }
            else if (selectedRegion.choiceB == 8)
            {
                textB.text = "Water changes from <b>clean</b> and <b>clear</b> to an <b>oxygen-deficient</b> state <b>naturally</b>, through a process called <b>eutrophication</b>. However, the process is <b>accelerated</b> by <b>pollution</b>. Issue laws against water pollution.";
            }

            //text for choiceC
            if (selectedRegion.choiceC == 3)
            {
                textC.text = "<b>Fracking</b> obtains <b>shale gas</b> and consumes large quantities of freshwater, returning that <b>water</b> in a <b>highly polluted</b> state. Often it will contain <b>chemical additives</b> and minerals that may include <b>toxic elements</b> such as <b>barium</b> and <b>radium</b>. Remove a fracking platform.";
            }
            else if (selectedRegion.choiceC == 6)
            {
                textC.text = "Above a certain <b>temperature</b> threshold, forests start to <b>leak</b> the <b>absorbed CO2</b>, back into <b>the atmosphere</b>. Issue a forest protection law in order to prevent the rise of temperatures in the area.";
            }
            else if (selectedRegion.choiceC == 9)
            {
                textC.text = "A <b>rise</b> in <b>temperature</b> in the water <b>decreases</b> its capacity to hold dissolved <b>oxygen</b>. The <b>power plant</b> uses <b>freshwater</b> for <b>cooling</b>. Construct additional features to prevent it from <b>dumping heated</b> into the nearby river.";
            }
        }
        //Destructive version
        else if (selectedRegion != null && versionManagerScript.isConstructive == false)
        {
            if (selectedRegion.choiceA == 1)
            {
                textA.text = "This area is rich with <b>shales</b>. <b>Shales</b> are <b>sedimentary rock</b> formations rich in organic matter and are often used for <b>extracting</b> a dry <b>natural gas</b> composed largely of <b>methane</b>, also known as shale gas. Build a <b>shale gas</b> extractor in the area.";
            }
            else if (selectedRegion.choiceA == 4)
            {
                textA.text = "Counteract <b>afforestation</b>, which is the <b>planting of new trees</b> in open land. ";
            }
            else if (selectedRegion.choiceA == 7)
            {
                textA.text = "The local lake is too clean and clear, meaning a factory that dumps its waste directly into it has to be built. This is also called polluting via a <b>point source</b>, since the pollutions stems from <b>one point only</b>. Build a factory in order to do <b>point source pollution.</b>";
            }

            //text for choiceB
            if (selectedRegion.choiceB == 2)
            {
                textB.text = "<b>Fracking</b> is a process in which large volumes of <b>freshwater</b>, <b>sand</b>, and <b>chemicals</b> are injected into the ground, which in turn <b>creates fractures</b> in shale formations, allowing the <b>shale gas</b> to escape and be recovered. Promote the use of fracking platforms across the area.";
            }
            else if (selectedRegion.choiceB == 5)
            {
                textB.text = "Increase the rate of <b>greenhouse gas</b> emissions by destroying the nearby forests, since they normally <b>absorb CO2</b> through <b>photosynthesis</b>.";
            }
            else if (selectedRegion.choiceB == 8)
            {
                textB.text = "Water changes from <b>clean</b> and <b>clear</b> to an <b>oxygen-deficient</b> state <b>naturally</b>, through a process called <b>eutrophication</b>. However, the process is <b>accelerated</b> by <b>pollution</b>. Build more industrial facilities that will dump their waste in the waters.";
            }

            //text for choiceC
            if (selectedRegion.choiceC == 3)
            {
                textC.text = "<b>Fracking</b> obtains <b>shale gas</b> and consumes large quantities of freshwater, returning that <b>water</b> in a <b>highly polluted</b> state. Often it will contain <b>chemical additives</b> and minerals that may include <b>toxic elements</b> such as <b>barium</b> and <b>radium</b>. Build a fracking platform in the area.";
            }
            else if (selectedRegion.choiceC == 6)
            {
                textC.text = "Above a certain <b>temperature</b> threshold, forests start to <b>leak</b> the <b>absorbed CO2</b> back into <b>the atmosphere</b>. Remove a forest protection law that prevents the rise of temperatures in the area.";
            }
            else if (selectedRegion.choiceC == 9)
            {
                textC.text = "A <b>rise</b> in <b>temperature</b> in the water <b>decreases</b> its capacity to hold dissolved <b>oxygen</b>. A <b>power plant</b> uses <b>freshwater</b> for <b>cooling</b>. Build a power plant that <b>dumps</b> the <b>heated water</b> into the nearby river.";
            }
        }
    }

    public void StartConstruction(float timeCountdown, int constructionChosen)
    {
        selectedContinent = selectedRegion.associatedContinent;

        if (selectedContinent.isBuilding == false)
        {
            selectedContinent.isBuilding = true;
            StartCoroutine(Choices(timeCountdown, constructionChosen, selectedRegion, selectedContinent));
            interactionScript.HideConstructionMenu();
        }
    }

    IEnumerator Choices(float timeCountdown, int constructionChosen, Region selectedRegion, ContinentMarker selectedContinent)
    {
        switch (constructionChosen)
        {
            case 0:
                Debug.Log("There is no countdown 0");
                break;
            //Choice A, B & C for region A
            case 1:
                selectedRegion.countdown = 120.0f;
                timeCountdown = 120.0f;
                HoverAPercentage.text = "+??%";
                HoverAPerYear.text = "(Yearly increase)";
                HoverATime.text = "???.??";
                if (versionManagerScript.isConstructive == true)
                {
                    SetMessage(b1, "Way to go! Issuing a protective law limits the exploitation of shale gas!");
                    b1 = true;
                }
                else
                {
                    SetMessage(b1, "Way to go! You have successfully built a shale gas extractor in the area!");
                    b1 = true;
                }
                break;
            case 2:
                selectedRegion.countdown = 10.0f;
                timeCountdown = 10.0f;
                HoverBPercentage.text = "+??%";
                HoverBPerYear.text = "(Yearly increase)";
                HoverBTime.text = "???.??";
                if (versionManagerScript.isConstructive == true)
                {
                    SetMessage(b2, "Great! You have successfully limited the use of fracking platforms across the area!");
                    b2 = true;
                }
                else
                {
                    SetMessage(b2, "Great! You have promoted the use of fracking platforms across the area!");
                    b2 = true;
                }
                break;
            case 3:
                selectedRegion.countdown = 10.0f;
                timeCountdown = 10.0f;
                HoverCPercentage.text = "+??%";
                HoverCPerYear.text = "(Yearly increase)";
                HoverCTime.text = "???.??";
                if (versionManagerScript.isConstructive == true)
                {
                    SetMessage(b3, "Super! You removed a fracking platform, thus preventing the pollution from the fracking process!");
                    b3 = true;
                }
                else
                {
                    SetMessage(b3, "Super! You built a fracking platform in the area, thus polluting the area from the fracking process!");
                    b3 = true;
                }
                break;
            //Choice A, B & C for region B
            case 4:
                selectedRegion.countdown = 120.0f;
                timeCountdown = 120.0f;
                HoverAPercentage.text = "+??%";
                HoverAPerYear.text = "(Yearly increase)";
                HoverATime.text = "???.??";
                if (versionManagerScript.isConstructive == true)
                {
                    SetMessage(b4, "Congratulations! You have successfully planted new trees, thus afforesting the area!");
                    b4 = true;
                }
                else
                {
                    SetMessage(b4, "Congratulations! You have successfully counteracted the planting of new trees, thus preventing afforestation in the area!");
                    b4 = true;
                }
                break;
            case 5:
                selectedRegion.countdown = 10.0f;
                timeCountdown = 10.0f;
                HoverBPercentage.text = "+??%";
                HoverBPerYear.text = "(Yearly increase)";
                HoverBTime.text = "???.??";
                if (versionManagerScript.isConstructive == true)
                {
                    SetMessage(b5, "Hurray! You slowed the rate of greenhouse gas emissions by planting more trees!");
                    b5 = true;
                }
                else
                {
                    SetMessage(b5, "Hurray! You increased the rate of greenhouse gas emissions by destroying more trees!");
                    b5 = true;
                }
                break;
            case 6:
                selectedRegion.countdown = 10.0f;
                timeCountdown = 10.0f;
                HoverCPercentage.text = "+??%";
                HoverCPerYear.text = "(Yearly increase)";
                HoverCTime.text = "???.??";
                if (versionManagerScript.isConstructive == true)
                {
                    SetMessage(b6, "Success! You managed to issue a forest protection law in order to prevent the rise of temperatures in the area!");
                    b6 = true;
                }
                else
                {
                    SetMessage(b6, "Success! You managed to remove a forest protection law in order to increase temperatures in the area!");
                    b6 = true;
                }
                break;
            //Choice A, B & C for region C
            case 7:
                selectedRegion.countdown = 120.0f;
                timeCountdown = 120.0f;
                HoverAPercentage.text = "+??%";
                HoverAPerYear.text = "(Yearly increase)";
                HoverATime.text = "???.??";
                if (versionManagerScript.isConstructive == true)
                {
                    SetMessage(b7, "Fantastic! You fined the factory to stop their point source pollution!");
                    b7 = true;
                }
                else
                {
                    SetMessage(b7, "Fantastic! You built a factory that does point source pollution!");
                    b7 = true;
                }
                break;
            case 8:
                selectedRegion.countdown = 10.0f;
                timeCountdown = 10.0f;
                HoverBPercentage.text = "+??%";
                HoverBPerYear.text = "(Yearly increase)";
                HoverBTime.text = "???.??";
                if (versionManagerScript.isConstructive == true)
                {
                    SetMessage(b8, "Good job! You have successfully issued laws against water pollution, thus slowing the process of eutrophication!");
                    b8 = true;
                }
                else
                {
                    SetMessage(b8, "Good job! You have successfully built more industrial facilities, thus accelerating the process of eutrophication!");
                    b8 = true;
                }
                break;
            case 9:
                selectedRegion.countdown = 10.0f;
                timeCountdown = 10.0f;
                HoverCPercentage.text = "+??%";
                HoverCPerYear.text = "(Yearly increase)";
                HoverCTime.text = "???.??";
                if (versionManagerScript.isConstructive == true)
                {
                    SetMessage(b9, "Awesome! You constructed additional features to the power plant, thus preventing it from dumping heated water into the nearby river!");
                    b9 = true;
                }
                else
                {
                    SetMessage(b9, "Awesome! You constructed a power plant, which dumps its heated water into the nearby river!");
                    b9 = true;
                }
                break;
            default:
                Debug.Log("Case not found, Countdowns");
                break;
        }

        //selectedRegion.countdown = timeCountdown;

        yield return new WaitForSeconds(timeCountdown);

        switch (constructionChosen)
        {
            case 0:
                Debug.Log("There is no choice 0");
                break;
            //Choice A, B & C for region A
            case 1:
                selectedRegion.percentage += 8.0f;
                selectedRegion.percentageIncrement += 0.0f;
                score.score += 500;
                selectedContinent.isBuilding = false;
                break;
            case 2:
                selectedRegion.percentage += 8.0f;
                selectedRegion.percentageIncrement += 0.0f;
                score.score += 500;
                selectedContinent.isBuilding = false;
                break;
            case 3:
                selectedRegion.percentage += 4.0f;
                selectedRegion.percentageIncrement += 0.0f;
                score.score += 500;
                selectedContinent.isBuilding = false;
                break;
            //Choice A, B & C for region B
            case 4:
                selectedRegion.percentage += 4.0f;
                selectedRegion.percentageIncrement += 0.0f;
                score.score += 500;
                selectedContinent.isBuilding = false;
                break;
            case 5:
                selectedRegion.percentage += 4.0f;
                selectedRegion.percentageIncrement += 0.0f;
                score.score += 500;
                selectedContinent.isBuilding = false;
                break;
            case 6:
                selectedRegion.percentage += 4.0f;
                selectedRegion.percentageIncrement += 0.0f;
                score.score += 500;
                selectedContinent.isBuilding = false;
                break;
                //Choice A, B & C for region C
            case 7:
                selectedRegion.percentage += 4.0f;
                selectedRegion.percentageIncrement += 0.0f;
                score.score += 500;
                selectedContinent.isBuilding = false;
                break;
            case 8:
                selectedRegion.percentage += 4.0f;
                selectedRegion.percentageIncrement += 0.0f;
                score.score += 500;
                selectedContinent.isBuilding = false;
                break;
            case 9:
                selectedRegion.percentage += 4.0f;
                selectedRegion.percentageIncrement += 0.0f;
                score.score += 500;
                selectedContinent.isBuilding = false;
                break;
            default:
                Debug.Log("Case not found, Choices");
                break;
        }
    }

    public void ShowBigHelper()
    {
        bigHelperOBJ = GameObject.Find("BigHelper");
        bigHelperCanvasGroup = bigHelperOBJ.GetComponent<CanvasGroup>();
        bigHelperCanvasGroup.alpha = 1;
        bigHelperCanvasGroup.interactable = true;
        bigHelperCanvasGroup.blocksRaycasts = true;
    }

    public void HideBigHelper()
    {
        interactionScript.regionIsOccluded = false;
        bigHelperOBJ = GameObject.Find("BigHelper");
        bigHelperCanvasGroup = bigHelperOBJ.GetComponent<CanvasGroup>();
        bigHelperCanvasGroup.alpha = 0;
        bigHelperCanvasGroup.interactable = false;
        bigHelperCanvasGroup.blocksRaycasts = false;
    }

    public void ShowMinimizedHelper()
    {
        minimizedHelpherOBJ = GameObject.Find("MinimizedHelper");
        minimizedHelperCanvasGroup = minimizedHelpherOBJ.GetComponent<CanvasGroup>();
        minimizedHelperCanvasGroup.alpha = 1;
        minimizedHelperCanvasGroup.interactable = true;
        minimizedHelperCanvasGroup.blocksRaycasts = true;
    }

    public void HideMinimizedHelper()
    {
        minimizedHelpherOBJ = GameObject.Find("MinimizedHelper");
        minimizedHelperCanvasGroup = minimizedHelpherOBJ.GetComponent<CanvasGroup>();
        minimizedHelperCanvasGroup.alpha = 0;
        minimizedHelperCanvasGroup.interactable = false;
        minimizedHelperCanvasGroup.blocksRaycasts = false;
    }

    public void SetMessage(bool inputB, string inputText)
    {
        if (inputB == false)
        {
            socialActorTextSmall.text = inputText;
            socialActorTextBig.text = socialActorTextSmall.text;
            ShowBigHelper();
            HideMinimizedHelper();
        }
                else
        {
            if (Random.Range(1, 4) == 1)
            {
                socialActorTextSmall.text = inputText;
                socialActorTextBig.text = socialActorTextSmall.text;
            }
        }
    }
}