﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RegionMarker : MonoBehaviour
{
    private Text percentageText, timeText;
    public float percentage, countdown;
    public GameObject regionObject, percentageTextOBJ, timeTextOBJ;
    public Region regionScript;


    // Use this for initialization
    void Start()
    {
        regionScript = regionObject.GetComponent<Region>();

        percentageText = percentageTextOBJ.GetComponent<Text>();

        if (timeTextOBJ != null)
        {
            timeText = timeTextOBJ.GetComponent<Text>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        percentage = regionScript.percentage;
        countdown = regionScript.countdown;

        if (countdown > 0)
        {
            countdown -= Time.deltaTime;
            if (timeText != null)
            {
                timeText.text = countdown.ToString("F2");
                percentageText.text = Mathf.Ceil(percentage) + "%";
            }
        }
        else
        {
            countdown = 0;
            if (timeText != null)
            {
                timeText.text = "";
            }
            percentageText.text = Mathf.Ceil(percentage) + "%";
        }
    }
}