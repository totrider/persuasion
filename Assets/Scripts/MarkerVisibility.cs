﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MarkerVisibility : MonoBehaviour
{
    private List<GameObject> regionList = new List<GameObject>(), continentList = new List<GameObject>(), regionCanvasList = new List<GameObject>(), continentCanvasList = new List<GameObject>();

    void Start ()
    {
        regionList.AddRange(GameObject.FindGameObjectsWithTag("Region"));
        continentList.AddRange(GameObject.FindGameObjectsWithTag("ContinentMarker"));

        foreach (GameObject element in regionList)
        {
            regionCanvasList.Add(element.transform.GetChild(0).GetChild(0).gameObject);
        }
        foreach (GameObject element in continentList)
        {
            continentCanvasList.Add(element.transform.GetChild(0).GetChild(0).gameObject);
        }
    }

    void Update ()
    {
        if (!Interaction.isZoomedIn)
        {
            foreach (GameObject element in regionList)
            {
                element.GetComponent<Renderer>().enabled = false;
                element.GetComponent<BoxCollider>().isTrigger = false;
                element.GetComponent<BoxCollider>().enabled = false;
            }
            foreach (GameObject element in continentList)
            {
                element.GetComponent<Renderer>().enabled = true;
                element.GetComponent<BoxCollider>().isTrigger = true;
                element.GetComponent<BoxCollider>().enabled = true;
            }
            foreach (GameObject element in regionCanvasList)
            {
                element.GetComponent<CanvasGroup>().alpha = 0f;
            }
            foreach (GameObject element in continentCanvasList)
            {
                element.GetComponent<CanvasGroup>().alpha = 1f;
            }
        }
        else
        {
            foreach (GameObject element in regionList)
            {
                element.GetComponent<Renderer>().enabled = true;
                element.GetComponent<BoxCollider>().isTrigger = true;
                element.GetComponent<BoxCollider>().enabled = true;
            }
            foreach (GameObject element in continentList)
            {
                element.GetComponent<Renderer>().enabled = false;
                element.GetComponent<BoxCollider>().isTrigger = false;
                element.GetComponent<BoxCollider>().enabled = false;
            }
            foreach (GameObject element in regionCanvasList)
            {
                element.GetComponent<CanvasGroup>().alpha = 1f;
            }
            foreach (GameObject element in continentCanvasList)
            {
                element.GetComponent<CanvasGroup>().alpha = 0f;
            }
        }
    }
}
