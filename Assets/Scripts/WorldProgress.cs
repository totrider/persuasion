﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldProgress : MonoBehaviour
{
    private Text text;
    public float percentage;
    public GameObject continentMarkerObject_1, continentMarkerObject_2, continentMarkerObject_3, continentMarkerObject_4, continentMarkerObject_5, continentMarkerObject_6, continentMarkerObject_7, continentMarkerObject_8, continentMarkerObject_9, continentMarkerObject_10;
    public ContinentMarker continentMarkerScript_1, continentMarkerScript_2, continentMarkerScript_3, continentMarkerScript_4, continentMarkerScript_5, continentMarkerScript_6, continentMarkerScript_7, continentMarkerScript_8, continentMarkerScript_9, continentMarkerScript_10;


    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();
        continentMarkerScript_1 = continentMarkerObject_1.GetComponent<ContinentMarker>();
        continentMarkerScript_2 = continentMarkerObject_2.GetComponent<ContinentMarker>();
        continentMarkerScript_3 = continentMarkerObject_3.GetComponent<ContinentMarker>();
        continentMarkerScript_4 = continentMarkerObject_4.GetComponent<ContinentMarker>();
        continentMarkerScript_5 = continentMarkerObject_5.GetComponent<ContinentMarker>();
        continentMarkerScript_6 = continentMarkerObject_6.GetComponent<ContinentMarker>();
        continentMarkerScript_7 = continentMarkerObject_7.GetComponent<ContinentMarker>();
        continentMarkerScript_8 = continentMarkerObject_8.GetComponent<ContinentMarker>();
        continentMarkerScript_9 = continentMarkerObject_9.GetComponent<ContinentMarker>();
        continentMarkerScript_10 = continentMarkerObject_10.GetComponent<ContinentMarker>();

    }

    // Update is called once per frame
    void Update()
    {
        percentage = (continentMarkerScript_1.percentage + continentMarkerScript_2.percentage + continentMarkerScript_3.percentage + continentMarkerScript_4.percentage + continentMarkerScript_5.percentage + continentMarkerScript_6.percentage + continentMarkerScript_7.percentage + continentMarkerScript_8.percentage + continentMarkerScript_9.percentage + continentMarkerScript_10.percentage) / 10.0f;
        text.text = Mathf.Ceil(percentage) + "%";
        PlayerPrefs.SetInt("PercentageTotal", (int)Mathf.Ceil(percentage));
    }
}