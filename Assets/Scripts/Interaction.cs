﻿using UnityEngine;
using System.Collections;

public class Interaction : MonoBehaviour
{

    private GameObject tempGameObject;
    private bool isInteracting = false;
    public bool regionIsOccluded = false;
    public Material originalMaterial;
    public float rayDistance = 1.0f, highLightFloor = 0.3f, highLightCeiling = 1.0f, emission, oscillation = 1, zoomSpeed = 1;
    public Color baseColor, finalColor, noColor;
    public Renderer rend;
    public static bool isZoomedIn;
    public GameObject planetRotationOrigin, constructionMenu;
    public ConstructionOptions constructionOptionsScript;
    public Region regionScript;
    public ContinentZoomCoords continentZoomCoords;
    public Vector3 currentZoomPosition, targetZoomposition, camOriginalPosition;
    public CanvasGroup constructionCanvasGroup;

    void Start()
    {
        targetZoomposition = transform.position;
        camOriginalPosition = transform.position;

        constructionOptionsScript = constructionMenu.GetComponent<ConstructionOptions>();
        constructionCanvasGroup = constructionMenu.GetComponent<CanvasGroup>();
    }

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, targetZoomposition, zoomSpeed * Time.deltaTime);
        transform.LookAt(planetRotationOrigin.transform);

        RaycastHit hit;
        Ray highlightRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(highlightRay.origin, highlightRay.direction, Color.green);

        emission = highLightFloor + Mathf.PingPong(Time.time * oscillation, highLightCeiling - highLightFloor);
        finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

        if (Physics.Raycast(highlightRay, out hit, rayDistance))
        {
            // call highlight check function
            Highlight(hit.collider.gameObject);

            // check if raycast is hitting Continent
            if (hit.collider.gameObject.tag == "Continent")
            {
                // store current gameobject as temp
                tempGameObject = hit.collider.gameObject;
                continentZoomCoords = tempGameObject.GetComponent<ContinentZoomCoords>();
                originalMaterial = tempGameObject.GetComponent<Renderer>().material;

                //Debug.Log("This is a continent");
                //change the gameObject shader - highlight

                if (isZoomedIn == false)
                {
                    rend = hit.collider.gameObject.GetComponent<Renderer>();
                    rend.material.shader = Shader.Find("Standard");
                    rend.material.SetColor("_EmissionColor", finalColor);
                }

                if (Input.GetAxis("Mouse ScrollWheel") > 0 && isZoomedIn == false)
                {
                    //Debug.Log("you zoomed in");

                    targetZoomposition = continentZoomCoords.zoomReferenceObject.transform.position;

                    rend = tempGameObject.GetComponent<Renderer>();
                    rend.material.shader = Shader.Find("Standard");
                    rend.material.SetColor("_EmissionColor", noColor);

                    isZoomedIn = true;
                    tempGameObject = null;
                }
            }

            // check if raycast is hitting Region
            if (hit.collider.gameObject.tag == "Region" && regionIsOccluded == false)
            {
                // store current gameObject as temp
                tempGameObject = hit.collider.gameObject;


                //Debug.Log("This is a region");
                rend = hit.collider.gameObject.GetComponent<Renderer>();

                rend.material.shader = Shader.Find("Standard");
                rend.material.SetColor("_EmissionColor", finalColor);

                //check if mouse button is pressed down
                if (Input.GetMouseButtonDown(0) && isZoomedIn == true)
                {
                    //Debug.Log("you clicked a region");

                    //set the selected region for the construction menu to whatever the raycaster has hit, this enables transfer of variables from the individual regions to the construction script.
                    regionScript = tempGameObject.GetComponent<Region>();
                    constructionOptionsScript.selectedRegion = regionScript;

                    if (regionScript.associatedContinent.isBuilding == false)
                    {
                        ShowConstructionMenu();
                    }
                    else
                    {
                        //Debug.Log("You are already building something");
                    }

                }
            }
        }
        else
            isInteracting = false;

        if (Input.GetAxis("Mouse ScrollWheel") < 0 && isZoomedIn == true)
        {
            //Debug.Log("you zoomed out");
            targetZoomposition = camOriginalPosition;
            HideConstructionMenu();

            isZoomedIn = false;
        }

        if (isInteracting == false && tempGameObject != null)
        {
            rend = tempGameObject.GetComponent<Renderer>();
            rend.material.shader = Shader.Find("Standard");
            rend.material.SetColor("_EmissionColor", noColor);

            tempGameObject = null;
        }
    }

    public void Highlight(GameObject input)
    {
        isInteracting = true;
        if (tempGameObject != input && tempGameObject != null)
        {
            rend = tempGameObject.GetComponent<Renderer>();
            rend.material.shader = Shader.Find("Standard");
            rend.material.SetColor("_EmissionColor", noColor);

            tempGameObject = null;
        }
    }

    public void HideConstructionMenu()
    {
        regionIsOccluded = false;
        constructionCanvasGroup.alpha = 0f; //this makes everything transparent
        constructionCanvasGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
        constructionCanvasGroup.interactable = false;
    }

    public void ShowConstructionMenu()
    {
        regionIsOccluded = true;
        constructionCanvasGroup.alpha = 1f;
        constructionCanvasGroup.blocksRaycasts = true;
        constructionCanvasGroup.interactable = true;
    }
}